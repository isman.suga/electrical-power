<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    protected $urladafruitio = 'https://io.adafruit.com/api/v2/ismansuga/feeds/';
    protected $iokey = 'aio_byIJ69Ka5D8w1m8XlAdUc8F0DdEE';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getData() {
        $w = 0;
        $w2 = 0;
        $w3 = 0;
        $kwh = 0;
        $kwh2 = 0;
        $kwh3 = 0;
        $jml = 0;
        $jml2 = 0;
        $jml3 = 0;
        $url = $this->urladafruitio . 'listrik-1/data';
        $url2 = $this->urladafruitio . 'listrik-2/data';
        $url3 = $this->urladafruitio . 'listrik-3/data';
        $separator = "#";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response_curl = curl_exec($curl);
        curl_close($curl);
        $response_curl = json_decode($response_curl, true);
        foreach($response_curl as $data) {
            $val = $data['value'];
            $date = strtotime($data['created_at']);
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                $w += $arr[0];
                $jml += $arr[1];
                if (date('Ym') == date('Ym', $date)) {
                    $kwh += $arr[1];
                }
            }
        }

        $curl2 = curl_init();
        curl_setopt($curl2, CURLOPT_URL, $url2);
        curl_setopt($curl2, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl2, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, true);
        $response_curl2 = curl_exec($curl2);
        curl_close($curl2);
        $response_curl2 = json_decode($response_curl2, true);
        foreach($response_curl2 as $data) {
            $val = $data['value'];
            $date = strtotime($data['created_at']);
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                $w2 += $arr[0];
                $jml2 += $arr[1];
                if (date('Ym') == date('Ym', $date)) {
                    $kwh2 += $arr[1];
                }
            }
        }

        $curl3 = curl_init();
        curl_setopt($curl3, CURLOPT_URL, $url3);
        curl_setopt($curl3, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($curl3, CURLOPT_POSTFIELDS, array('X-AIO-Key' => $this->iokey));
        curl_setopt($curl3, CURLOPT_RETURNTRANSFER, true);
        $response_curl3 = curl_exec($curl3);
        curl_close($curl3);
        $response_curl3 = json_decode($response_curl3, true);
        foreach($response_curl3 as $data) {
            $val = $data['value'];
            $date = strtotime($data['created_at']);
            if(strpos($val, $separator) !== false) {
                $arr = explode($separator, $val);
                $w3 += $arr[0];
                $jml3 += $arr[1];
                if (date('Ym') == date('Ym', $date)) {
                    $kwh3 += $arr[1];
                }
            }
        }

        $data = array (
            "sensor1" => array("watt" => $w, "kwh" => $kwh, "total" => $jml),
            "sensor2" => array("watt" => $w2, "kwh" => $kwh2, "total" => $jml2),
            "sensor3" => array("watt" => $w3, "kwh" => $kwh3, "total" => $jml3)
        );

        return $data;
    }

}
