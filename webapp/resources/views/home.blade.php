@extends('layouts.app')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Data Pemakaian Pelanggan</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 1</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-sm-7">
                                    <strong>Total Pemakaian Kwh</strong><br>
                                    <div id="tot-kwh-1">0 KWH</div>
                                    <strong>Jumlah Pemakaian Kwh Bulan Ini</strong><br>
                                    <div id="kwh-1">0 KWH</div>
                                </div>
                                <div class="col-sm-5">
                                    <strong>Daya Watt</strong><br>
                                    <div id="w-1">0 W</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 2</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-sm-7">
                                    <strong>Total Pemakaian Kwh</strong><br>
                                    <div id="tot-kwh-2">0 KWH</div>
                                    <strong>Jumlah Pemakaian Kwh Bulan Ini</strong><br>
                                    <div id="kwh-2">0 KWH</div>
                                </div>
                                <div class="col-sm-5">
                                    <strong>Daya Watt</strong><br>
                                    <div id="w-2">0 W</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Pelanggan 3</h5>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                    <i class="fas fa-minus"></i>
                                </button>
                                <button type="button" class="btn btn-tool" data-card-widget="remove">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row invoice-info">
                                <div class="col-sm-7">
                                    <strong>Total Pemakaian Kwh</strong><br>
                                    <div id="tot-kwh-3">0 KWH</div>
                                    <strong>Jumlah Pemakaian Kwh Bulan Ini</strong><br>
                                    <div id="kwh-3">0 KWH</div>
                                </div>
                                <div class="col-sm-5">
                                    <strong>Daya Watt</strong><br>
                                    <div id="w-3">0 W</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content -->
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        var total = document.getElementById("tot-kwh-1");
        var kwh = document.getElementById("kwh-1");
        var watt = document.getElementById("w-1");
        var total2 = document.getElementById("tot-kwh-2");
        var kwh2 = document.getElementById("kwh-2");
        var watt2 = document.getElementById("w-2");
        var total3 = document.getElementById("tot-kwh-3");
        var kwh3 = document.getElementById("kwh-3");
        var watt3 = document.getElementById("w-3");

        function reloadData() {
            $.ajax({
                url: "{{ route('data') }}",
                type: "GET",
                data: {
                    format: 'json',
                    _token:"{{ csrf_token() }}"
                },
                sasdataType: 'json',
                success: function(response){
                    $.each(response, function(index, item){
                        if (index == "sensor1") {
                            total.innerHTML = item['total'] + " KWH";
                            kwh.innerHTML = item['kwh'] + " KWH";
                            watt.innerHTML = item['watt'] + " W";
                        }
                        if (index == "sensor2") {
                            total2.innerHTML = item['total'] + " KWH";
                            kwh2.innerHTML = item['kwh'] + " KWH";
                            watt2.innerHTML = item['watt'] + " W";
                        }
                        if (index == "sensor3") {
                            total3.innerHTML = item['total'] + " KWH";
                            kwh3.innerHTML = item['kwh'] + " KWH";
                            watt3.innerHTML = item['watt'] + " W";
                        }
                    });
                    console.log(response);
                }
            });
        }

        setInterval(function () {
            reloadData();
        }, 10000); // update data di web setiap 10 detik
    });
</script>
@endpush